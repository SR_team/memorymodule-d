module memorymodule.getproc; 

private import memorymodule.base;  

FARPROC MemoryGetProcAddress(HMEMORYMODULE mod, const char *name)
{
	byte *codeBase = (cast(PMEMORYMODULE)mod).codeBase;
	int idx=-1;
	DWORD i;
	DWORD* nameRef;
	WORD *ordinal;
	PIMAGE_EXPORT_DIRECTORY exports;
	PIMAGE_DATA_DIRECTORY directory = &(cast(PMEMORYMODULE)mod).headers.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT];
	if (directory.Size == 0) {
		// no export table found
		return NULL;
	}

	exports = cast(PIMAGE_EXPORT_DIRECTORY) (codeBase + directory.VirtualAddress);
	if (exports.NumberOfNames == 0 || exports.NumberOfFunctions == 0) {
		// DLL doesn't export anything
		return NULL;
	}

	// search function name in list of exported names
	nameRef = cast(DWORD *) (codeBase + exports.AddressOfNames);
	ordinal = cast(WORD *) (codeBase + exports.AddressOfNameOrdinals);
	for (i=0; i<exports.NumberOfNames; i++, nameRef++, ordinal++) {
		if (strcmp(name, cast(const(char*)) (codeBase + (*nameRef))) == 0) {
			idx = *ordinal;
			break;
		}
	}

	if (idx == -1) {
		// exported symbol not found
		return NULL;
	}

	if (cast(DWORD)idx > exports.NumberOfFunctions) {
		// name <. ordinal number don't match
		return NULL;
	}

	// AddressOfFunctions contains the RVAs to the "real" functions
	return cast(FARPROC) (codeBase + (*cast(DWORD *) (codeBase + exports.AddressOfFunctions + (idx*4))));
}
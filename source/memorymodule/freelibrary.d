module memorymodule.freelibrary; 

private import memorymodule.base; 

void MemoryFreeLibrary(HMEMORYMODULE mod_)
{
	int i;
	PMEMORYMODULE mod = cast(PMEMORYMODULE)mod_;

	if (mod != NULL) {
		if (mod.initialized != 0) {
			// notify library about detaching from process
			DllEntryProc DllEntry = cast(DllEntryProc) (mod.codeBase + mod.headers.OptionalHeader.AddressOfEntryPoint);
			DllEntry(cast(HINSTANCE)mod.codeBase, DLL_PROCESS_DETACH, null);
			mod.initialized = 0;
		}

		if (mod.modules != NULL) {
			// free previously opened libraries
			for (i=0; i<mod.numModules; i++) {
				if (mod.modules[i] != INVALID_HANDLE_VALUE) {
					FreeLibrary(mod.modules[i]);
				}
			}

			free(mod.modules);
		}

		if (mod.codeBase != NULL) {
			// release memory of library
			VirtualFree(mod.codeBase, 0, MEM_RELEASE);
		}

		HeapFree(GetProcessHeap(), 0, mod);
	}
}
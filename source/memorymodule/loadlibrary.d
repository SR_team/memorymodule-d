module memorymodule.loadlibrary; 

private import memorymodule.base, memorymodule.freelibrary;

HMEMORYMODULE MemoryLoadLibrary(const void *data)
{
	PMEMORYMODULE result;
	PIMAGE_DOS_HEADER dos_header;
	PIMAGE_NT_HEADERS old_header;
	byte* code, headers;
	SIZE_T locationDelta;
	DllEntryProc DllEntry;
	BOOL successfull;

	dos_header = cast(PIMAGE_DOS_HEADER)data;
	if (dos_header.e_magic != IMAGE_DOS_SIGNATURE) {
		return NULL;
	}

	old_header = cast(PIMAGE_NT_HEADERS)&(cast(const(byte*))(data))[dos_header.e_lfanew];
	if (old_header.Signature != IMAGE_NT_SIGNATURE) {
		return NULL;
	}

	// reserve memory for image of library
	code = cast(byte *)VirtualAlloc(cast(LPVOID)old_header.OptionalHeader.ImageBase,
		old_header.OptionalHeader.SizeOfImage,
		MEM_RESERVE,
		PAGE_READWRITE);

    if (code == NULL) {
        // try to allocate memory at arbitrary position
        code = cast(byte *)VirtualAlloc(NULL,
            old_header.OptionalHeader.SizeOfImage,
            MEM_RESERVE,
            PAGE_READWRITE);
		if (code == NULL) {
			return NULL;
		}
	}
    
	result = cast(PMEMORYMODULE)HeapAlloc(GetProcessHeap(), 0, MEMORYMODULE.sizeof);
	result.codeBase = code;
	result.numModules = 0;
	result.modules = NULL;
	result.initialized = 0;

	// XXX: is it correct to commit the complete memory region at once?
    //      calling DllEntry raises an exception if we don't...
	VirtualAlloc(code,
		old_header.OptionalHeader.SizeOfImage,
		MEM_COMMIT,
		PAGE_READWRITE);

	// commit memory for headers
	headers = cast(byte *)VirtualAlloc(code,
		old_header.OptionalHeader.SizeOfHeaders,
		MEM_COMMIT,
		PAGE_READWRITE);
	
	// copy PE header to code
	memcpy(headers, dos_header, dos_header.e_lfanew + old_header.OptionalHeader.SizeOfHeaders);
	result.headers = cast(PIMAGE_NT_HEADERS)&(cast(const(byte*))(headers))[dos_header.e_lfanew];

	// update position
	result.headers.OptionalHeader.ImageBase = cast(POINTER_TYPE)code;

	// copy sections from DLL file block to new memory location
	CopySections(cast(const(byte*))data, old_header, result);

	// adjust base address of imported data
	locationDelta = cast(SIZE_T)(code - old_header.OptionalHeader.ImageBase);
	if (locationDelta != 0) {
		PerformBaseRelocation(result, locationDelta);
	}

	// load required dlls and adjust function table of imports
	if (!BuildImportTable(result)) {
		goto error;
	}

	// mark memory pages depending on section headers and release
	// sections that are marked as "discardable"
	FinalizeSections(result);

	// get entry point of loaded library
	if (result.headers.OptionalHeader.AddressOfEntryPoint != 0) {
		DllEntry = cast(DllEntryProc) (code + result.headers.OptionalHeader.AddressOfEntryPoint);
		if (DllEntry is null) {
			goto error;
		}

		// notify library about attaching to process
		successfull = DllEntry(cast(HINSTANCE)code, DLL_PROCESS_ATTACH, null);
		if (!successfull) {
			goto error;
		}
		result.initialized = 1;
	}

	return cast(HMEMORYMODULE)result;

error:
	// cleanup
	MemoryFreeLibrary(result);
	return NULL;
}
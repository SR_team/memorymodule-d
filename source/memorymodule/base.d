module memorymodule.base;

version (Windows):
public:

import core.sys.windows.windows, core.sys.windows.winnt;
import core.stdc.string, core.stdc.stdlib, std.conv : to;

alias HMEMORYMODULE = void*;

package:
struct MEMORYMODULE {
	PIMAGE_NT_HEADERS headers;
	byte* codeBase;
	HMODULE *modules;
	int numModules;
	int initialized;
}
alias PMEMORYMODULE = MEMORYMODULE*;

version(Win64){
	alias POINTER_TYPE = ulong;
} else {
	alias POINTER_TYPE = uint;
}

alias DllEntryProc = extern (Windows) BOOL function(HINSTANCE, DWORD, LPVOID) @nogc nothrow pure @safe;

void CopySections(const(byte) *data, PIMAGE_NT_HEADERS old_headers, PMEMORYMODULE mod)
{
	int i, size;
	byte *codeBase = mod.codeBase;
	byte *dest;
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(mod.headers);
	for (i=0; i< mod.headers.FileHeader.NumberOfSections; i++, section++) {
		if (section.SizeOfRawData == 0) {
			// section doesn't contain data in the dll itself, but may define
			// uninitialized data
			size = old_headers.OptionalHeader.SectionAlignment;
			if (size > 0) {
				dest = cast(byte*)VirtualAlloc(codeBase + section.VirtualAddress,
					size,
					MEM_COMMIT,
					PAGE_READWRITE);

				section.Misc.PhysicalAddress = cast(POINTER_TYPE)dest;
				memset(dest, 0, size);
			}

			// section is empty
			continue;
		}

		// commit memory block and copy data from dll
		dest = cast(byte*)VirtualAlloc(codeBase + section.VirtualAddress,
							section.SizeOfRawData,
							MEM_COMMIT,
							PAGE_READWRITE);
		memcpy(dest, data + section.PointerToRawData, section.SizeOfRawData);
		section.Misc.PhysicalAddress = cast(POINTER_TYPE)dest;
	}
}
// Protection flags for memory pages (Executable, Readable, Writeable)
__gshared int[2][2][2] ProtectionFlags = [
	[
		// not executable
		[PAGE_NOACCESS, PAGE_WRITECOPY],
		[PAGE_READONLY, PAGE_READWRITE],
	], [
		// executable
		[PAGE_EXECUTE, PAGE_EXECUTE_WRITECOPY],
		[PAGE_EXECUTE_READ, PAGE_EXECUTE_READWRITE],
	],
];
void FinalizeSections(PMEMORYMODULE mod)
{
	int i;
	PIMAGE_SECTION_HEADER section = IMAGE_FIRST_SECTION(mod.headers);
	version(Win64){
		POINTER_TYPE imageOffset = (mod.headers.OptionalHeader.ImageBase & 0xffffffff00000000);
	} else {
		POINTER_TYPE imageOffset = 0;
	}
	
	// loop through all sections and change access flags
	for (i=0; i<mod.headers.FileHeader.NumberOfSections; i++, section++) {
		DWORD protect, oldProtect, size;
		int executable = (section.Characteristics & IMAGE_SCN_MEM_EXECUTE) != 0;
		int readable =   (section.Characteristics & IMAGE_SCN_MEM_READ) != 0;
		int writeable =  (section.Characteristics & IMAGE_SCN_MEM_WRITE) != 0;

		if (section.Characteristics & IMAGE_SCN_MEM_DISCARDABLE) {
			// section is not needed any more and can safely be freed
			VirtualFree(cast(LPVOID)(cast(POINTER_TYPE)section.Misc.PhysicalAddress | imageOffset), section.SizeOfRawData, MEM_DECOMMIT);
			continue;
		}

		// determine protection flags based on characteristics
		protect = ProtectionFlags[executable][readable][writeable];
		if (section.Characteristics & IMAGE_SCN_MEM_NOT_CACHED) {
			protect |= PAGE_NOCACHE;
		}

		// determine size of region
		size = section.SizeOfRawData;
		if (size == 0) {
			if (section.Characteristics & IMAGE_SCN_CNT_INITIALIZED_DATA) {
				size = mod.headers.OptionalHeader.SizeOfInitializedData;
			} else if (section.Characteristics & IMAGE_SCN_CNT_UNINITIALIZED_DATA) {
				size = mod.headers.OptionalHeader.SizeOfUninitializedData;
			}
		}

		if (size > 0) {
			// change memory access flags
			VirtualProtect(cast(LPVOID)(cast(POINTER_TYPE)section.Misc.PhysicalAddress | imageOffset), size, protect, &oldProtect);
		}
	}
}

void PerformBaseRelocation(PMEMORYMODULE mod, SIZE_T delta)
{
	DWORD i;
	byte *codeBase = mod.codeBase;

	PIMAGE_DATA_DIRECTORY directory = &(mod).headers.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_BASERELOC];
	if (directory.Size > 0) {
		PIMAGE_BASE_RELOCATION relocation = cast(PIMAGE_BASE_RELOCATION) (codeBase + directory.VirtualAddress);
		for (; relocation.VirtualAddress > 0; ) {
			byte *dest = codeBase + relocation.VirtualAddress;
			ushort *relInfo = cast(ushort *)(cast(byte *)relocation + IMAGE_SIZEOF_BASE_RELOCATION);
			for (i=0; i<((relocation.SizeOfBlock-IMAGE_SIZEOF_BASE_RELOCATION) / 2); i++, relInfo++) {
				DWORD *patchAddrHL;
				version(Win64){
					ULONGLONG *patchAddr64;
				}
				int type, offset;

				// the upper 4 bits define the type of relocation
				type = *relInfo >> 12;
				// the lower 12 bits define the offset
				offset = *relInfo & 0xfff;
				
				switch (type)
				{
				case IMAGE_REL_BASED_ABSOLUTE:
					// skip relocation
					break;

				case IMAGE_REL_BASED_HIGHLOW:
					// change complete 32 bit address
					patchAddrHL = cast(DWORD *) (dest + offset);
					*patchAddrHL += delta;
					break;
				
				version(Win64){
					case IMAGE_REL_BASED_DIR64:
						patchAddr64 = cast(ULONGLONG *) (dest + offset);
						*patchAddr64 += delta;
						break;
				}

				default:
					//printf("Unknown relocation: %d\n", type);
					break;
				}
			}

			// advance to next relocation block
			relocation = cast(PIMAGE_BASE_RELOCATION) ((cast(char *) relocation) + relocation.SizeOfBlock);
		}
	}
}

int BuildImportTable(PMEMORYMODULE mod)
{
	int result=1;
	byte *codeBase = mod.codeBase;

	PIMAGE_DATA_DIRECTORY directory = &(mod).headers.OptionalHeader.DataDirectory[IMAGE_DIRECTORY_ENTRY_IMPORT];
	if (directory.Size > 0) {
		PIMAGE_IMPORT_DESCRIPTOR importDesc = cast(PIMAGE_IMPORT_DESCRIPTOR) (codeBase + directory.VirtualAddress);
		for (; !IsBadReadPtr(importDesc, IMAGE_IMPORT_DESCRIPTOR.sizeof) && importDesc.Name; importDesc++) {
			POINTER_TYPE *thunkRef;
			FARPROC *funcRef;
			HMODULE handle = LoadLibraryA(cast(LPCSTR) (codeBase + importDesc.Name));
			if (handle == NULL) {
				result = 0;
				break;
			}

			mod.modules = cast(HMODULE *)realloc(mod.modules, (mod.numModules+1)*(HMODULE.sizeof));
			if (mod.modules == NULL) {
				result = 0;
				break;
			}

			mod.modules[mod.numModules++] = handle;
			if (importDesc.OriginalFirstThunk) {
				thunkRef = cast(POINTER_TYPE *) (codeBase + importDesc.OriginalFirstThunk);
				funcRef = cast(FARPROC *) (codeBase + importDesc.FirstThunk);
			} else {
				// no hint table
				thunkRef = cast(POINTER_TYPE *) (codeBase + importDesc.FirstThunk);
				funcRef = cast(FARPROC *) (codeBase + importDesc.FirstThunk);
			}
			for (; *thunkRef; thunkRef++, funcRef++) {
				if (IMAGE_SNAP_BY_ORDINAL(*thunkRef)) {
					*funcRef = cast(FARPROC)GetProcAddress(handle, cast(LPCSTR)IMAGE_ORDINAL(*thunkRef));
				} else {
					PIMAGE_IMPORT_BY_NAME thunkData = cast(PIMAGE_IMPORT_BY_NAME) (codeBase + (*thunkRef));
					*funcRef = cast(FARPROC)GetProcAddress(handle, cast(LPCSTR)thunkData.Name);
				}
				if (*funcRef is null) {
					result = 0;
					break;
				}
			}

			if (!result) {
				break;
			}
		}
	}

	return result;
}